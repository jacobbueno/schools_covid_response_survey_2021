---
title: "printing_for_report"
author: "Jacob Bueno de Mesquita"
date: "3/22/2022"
output:
  html_document: default
  pdf_document: default
  word_document: default
always_allow_html: yes
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
library(tidyverse)
library(kableExtra)
library(knitr)
library(tinytex)
opts_knit$set(root.dir = "/Users/jacob/Desktop/git_repos/schools_covid_response_survey_2021/dfs")

```

## Set up to print tables
```{r loading, echo = FALSE}
setwd("/Users/jacob/Desktop/git_repos/schools_covid_response_survey_2021/dfs")
Q1_long_df <- readRDS(file = "Q1_long_df.RDS")
Q2_long_df <- readRDS(file = "Q2_long_df.RDS")
Q3_long_df <- readRDS(file = "Q3_long_df.RDS")
Q4_long_df <- readRDS(file = "Q4_long_df.RDS")
Q5_long_df <- readRDS(file = "Q5_long_df.RDS")
Q6_long_df <- readRDS(file = "Q6_long_df.RDS")
Q7_long_df <- readRDS(file = "Q7_long_df.RDS")
Q8_long_df <- readRDS(file = "Q8_long_df.RDS")
Q10_long_df <- readRDS(file = "Q10_long_df.RDS")
Q11_long_df <- readRDS(file = "Q11_long_df.RDS")
Q12_long_df <- readRDS(file = "Q12_long_df.RDS")
Q13_long_df <- readRDS(file = "Q13_long_df.RDS")
Q14_long_df <- readRDS(file = "Q14_long_df.RDS")
Q15_long_df <- readRDS(file = "Q15_long_df.RDS")
Q17_long_df <- readRDS(file = "Q17_long_df.RDS")
Q18_long_df <- readRDS(file = "Q18_long_df.RDS")
Q19_long_df <- readRDS(file = "Q19_long_df.RDS")
Q20_long_df <- readRDS(file = "Q20_long_df.RDS")
Q22_long_df <- readRDS(file = "Q22_long_df.RDS")
Q23_long_df <- readRDS(file = "Q23_long_df.RDS")
Q24_long_df <- readRDS(file = "Q24_long_df.RDS")
```

## Printing tables with survey response descriptive statistics

```{r tables, echo = FALSE}

tab_prod <- function (tab_data) {
        kable(tab_data, align = "c") %>%
                 kable_styling(c("striped", "bordered", "hover", "condensed", "responsive"),
                               full_width = FALSE, position = "center", font_size = 10) %>%
                 kable_styling(full_width = F) %>%
                 column_spec(1, bold = T) 
}
```

### Question 1. To what extent has your school district implemented the following ventilation and filtration strategies to support in-person instruction in response to the pandemic?
```{r q1, echo = FALSE}
tab_prod(Q1_long_df)
```

### Question 2: To what extent has your school district implemented the following monitoring and assessment activities to support in-person instruction in response to the pandemic?
```{r q2, echo = FALSE}
tab_prod(Q2_long_df)
```

### Question 3. During the current school year, have schools in your district implemented other behavioral strategies to support in-person instruction during the pandemic?
```{r q3, echo = FALSE}
tab_prod(Q3_long_df)
```

### Question 4. During the current school year, have schools in your district implemented other administrative strategies to support in-person instruction during the pandemic?
```{r q4, echo = FALSE}
tab_prod(Q4_long_df)
```

### Question 5. Are your schools using upper-room ultraviolet germicidal irradiation (UVGI; also referred to as germicidal UV or GUV)?
```{r q5, echo = FALSE}
tab_prod(Q5_long_df)
```

### Question 6. Are your schools using in-duct ultraviolet germicidal irradiation (UVGI; also referred to as germicidal UV or GUV)?
```{r q6, echo = FALSE}
tab_prod(Q6_long_df)
```

### Question 7. Are your schools using bipolar ionization or other ionizers?
```{r q7, echo = FALSE}
tab_prod(Q7_long_df)
```

### Question 8. Are your schools using other air cleaning technologies not described above?
```{r q8, echo = FALSE}
tab_prod(Q8_long_df)
```

### Question 10. What resources or guidance have you used to inform your decisions about which ventilation, filtration, and other building controls to implement in your buildings? Check all that apply.
```{r q10, echo = FALSE}
tab_prod(Q10_long_df)
```

### Question 11. If you selected “consultants or qualified professionals” in the response above, what group(s) have you engaged with to inform your decisions about ventilation, filtration, and other building controls? Check all that apply.
```{r q11, echo = FALSE}
tab_prod(Q11_long_df)
```

### Question 12. Who among your school community were involved in deciding which ventilation, filtration, and other building controls to implement in your buildings? Check all that apply.
```{r q12, echo = FALSE}
tab_prod(Q12_long_df)
```

### Question 13. How has your district funded or how does it plan to fund changes related to ventilation, filtration, and other building controls in your schools? Check all that apply.
```{r q13, echo = FALSE}
tab_prod(Q13_long_df)
```

### Question 14. What are the overall costs/savings from changes related to ventilation, filtration, and other building controls on the costs of operating your buildings, compared with the same period during a typical school year prior to the pandemic? Costs/savings may include energy, materials, and staffing.
```{r q14, echo = FALSE}
tab_prod(Q14_long_df)
```

### Question 15. How would you rate the overall effectiveness of ventilation, filtration, and other building controls implemented in your schools in controlling infection and supporting in-person instruction during the pandemic? We are interested in your judgment of effectiveness based on experience and/or data.
```{r q15, echo = FALSE}
tab_prod(Q15_long_df)
```

### Question 17. What challenges have schools in your district encountered when implementing, or considering implementing strategies to increase outside air ventilation involving HVAC systems during the pandemic? Check all that apply.
```{r q17, echo = FALSE}
tab_prod(Q17_long_df)
```

### Question 18. What challenges have schools in your district encountered when upgrading, or considering upgrading, to higher MERV air filters in your HVAC systems during the pandemic? Check all that apply.
```{r q18, echo = FALSE}
tab_prod(Q18_long_df)
```

### Question 19. What challenges have schools in your district encountered when installing, or considering installing in-room (including portable) air cleaners with HEPA/high efficiency filters during the pandemic? Check all that apply.
```{r q19, echo = FALSE}
tab_prod(Q19_long_df)
```

### Question 20. What challenges have schools in your district encountered when increasing, or considering increasing window opening during the pandemic? Check all that apply.
```{r q20, echo = FALSE}
tab_prod(Q20_long_df)
```

### Question 22. Looking ahead, is your school district planning to implement additional ventilation and filtration strategies, or make other building changes, to support in-person instruction during the pandemic? Check all that apply.
```{r q22, echo = FALSE}
tab_prod(Q22_long_df)
```

### Question 23. Does your district have access to funding to implement additional ventilation and filtration strategies, or make other building changes, in your schools?
```{r q23, echo = FALSE}
tab_prod(Q23_long_df)
```

### Question 24. What benefits, beyond reducing COVID-19 risk, do you see from the ventilation, filtration, and/or other building controls that were implemented in response to the pandemic? (check all that apply)
```{r q24, echo = FALSE}
tab_prod(Q24_long_df)
```


