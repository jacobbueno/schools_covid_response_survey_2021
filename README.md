# schools_covid_response_survey_2021

Survey on what schools are doing in response to COVID-19 pandemic. 

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://gitlab.com/-/experiment/new_project_readme_content:a3015eef83259c8e6c85b4f424b55688?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://gitlab.com/-/experiment/new_project_readme_content:a3015eef83259c8e6c85b4f424b55688?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://gitlab.com/-/experiment/new_project_readme_content:a3015eef83259c8e6c85b4f424b55688?https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/jacobbueno/schools_covid_response_survey_2021.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/-/experiment/new_project_readme_content:a3015eef83259c8e6c85b4f424b55688?https://gitlab.com/jacobbueno/schools_covid_response_survey_2021/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://gitlab.com/-/experiment/new_project_readme_content:a3015eef83259c8e6c85b4f424b55688?https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://gitlab.com/-/experiment/new_project_readme_content:a3015eef83259c8e6c85b4f424b55688?https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://gitlab.com/-/experiment/new_project_readme_content:a3015eef83259c8e6c85b4f424b55688?https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://gitlab.com/-/experiment/new_project_readme_content:a3015eef83259c8e6c85b4f424b55688?https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://gitlab.com/-/experiment/new_project_readme_content:a3015eef83259c8e6c85b4f424b55688?https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://gitlab.com/-/experiment/new_project_readme_content:a3015eef83259c8e6c85b4f424b55688?https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://gitlab.com/-/experiment/new_project_readme_content:a3015eef83259c8e6c85b4f424b55688?https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://gitlab.com/-/experiment/new_project_readme_content:a3015eef83259c8e6c85b4f424b55688?https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://gitlab.com/-/experiment/new_project_readme_content:a3015eef83259c8e6c85b4f424b55688?https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://gitlab.com/-/experiment/new_project_readme_content:a3015eef83259c8e6c85b4f424b55688?https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!).  Thank you to [makeareadme.com](https://www.makeareadme.com) for this template.

***

## Name
K-12 schools covid response survey fall 2021 responses

## Description
National survey of K-12 schools asking about measures that were taken to mitigate COVID transmission while maintaining in-person instruction, challenges to mitigation implementation, and perceived benefits and future plans. 


## Usage
There are several scripts associated with this project. Use them in order to reproduce the results in the manuscript.
1) survey_analysis.R: read in data, exploratory data analysis and cleaning, merging data, and production of analytic datasets, and plots of survey responses
2) school_char.R: exploring relationships between school district-level population variables and survey responses
3) analysis_2.R: updated analysis for report.
4) clusters.R: performs PAM and provides a draft run through of the analysis pipeline. For an updated analysis using the clusters identified in this clusters.R script, use the scripts described below: cluster_response_testing_reftownrural.R and cluster_response_testing_refsmallsub.R. 
5) cluster_response_testing_reftownrural.R: Use to prepare the final results for the Clustering analysis manuscript using dfs produced in the preceding scripts. Taking the town/rural communities as the reference group.
6) cluster_response_testing_refsmallsub.R: Use to prepare the final results for the Clustering analysis manuscript using dfs produced in the preceding scripts. Taking the smaller suburban communities as the reference group.

extras:
6) analysis_streamlined.R: taking just the essential analyses for reporting ease - can feed into markdown, etc. Clustering analysis was not included in this.
7) survey_relationships.R: exploring relationships between survey responses (not needed for final results)
8) draft_heatmap.R: exploring use of heatmap tool - potential for later integration into school_char and/or survey_relationships scripts
9) draft_likert_package.R: testing likert package
10) draft_test_hh_package_function.R: testing hh package function
11) draft_analysis: draft script to try/test out techniques


## Support
For questions, please contact Jacob Bueno de Mesquita at jbueno@lbl.gov. 

## Roadmap
May add additional qualitative analysis using specialized software.

## Contributing
Closed to contributions at this time

## Authors and acknowledgment
Analysis and scripting done by Jacob Bueno de Mesquita

## License
Please cite this work done by Center for Green Schools and Lawrence Berkeley National Lab.

## Project status
In progress.

